// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// Playground - noun: a place where people can play

//------------------------------------------------------
//Lesson 1 - 4/6/2014

// Boring way
var j = 20
for i in 5..10
{
    j * i
}

//Fun way
var 😗 = 4
var 🎃 = 9
var 😍 = 0
for 🐶 in 0..6
{
    😍 = 😗 * 🐶 * 🐶;
}
//------------------------------------------------------

//------------------------------------------------------
//Lesson 2 - 4/6/2014

//Java way
//static String myString = "Hello";

//Swift way
let myString = "Hello"

//More descriptive, but no need
//The :String will make the constant type safe
let myString2 : String = "Hello"

//What if I don't initialize the constant
//let myString3 : String

//The error says, you can't asign to 'let'.
//myString2 = "Hello"

//String variable with initialization
var welcomeMessage : String = "Hello"

//The numbers could be Int, UInt, Float, Double
var myNumber : Int = 34

//Something wrong with this?
var myStringNil

//This is assigning an optional type.
//But here the 'myStringNil2' variable is automatically set to Nil
var myStringNil2 : String?

/*NOTE

Swift’s nil is not the same as nil in Objective-C. In Objective-C, nil is a pointer to a non-existent object. In Swift, nil is not a pointer—it is the absence of a value of a certain type. Optionals of any type can be set to nil, not just object types.
*/
//------------------------------------------------------



//Vehicle class
class 🚙 {
    var ＃🎡 : Int
    var max👫 : Int
    func description() -> String {
        return "\(＃🎡) wheels; up to \(max👫) passengers"
    }
    init() {
        ＃🎡 = 0
        max👫 = 1
    }
}

//Inheritance
class 🚗 : 🚙 {
    init() {
        
    }
}
